---
layout: post
title:  "Gitlab runners"
date:   2022-03-22
tags: [Gitlab, Gitlab-runner]
---

Gitlab CI uses runners. On gitlab.com, the docker runner are just available. For your own gitlab instances, or if you have special needs, you need to set op your own.

See also the [official documentation](https://gitlab.com/moozer/runner-test).

There is an issue with the design that makes scripting difficult.

Docker runners
-----

To use, I made an ansible role available [here](https://gitlab.com/moozer/ansible-role-gitlab-runner-docker), and an associated [test project](https://gitlab.com/moozer/runner-test).

See the [readme](https://gitlab.com/moozer/ansible-role-gitlab-runner-docker/-/blob/master/README.md) for details.

This is set up as "specific runners". No monitoring, scaling, firewalling or similar needed to deploy in production is not implemented.




