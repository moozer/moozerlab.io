---
layout: post
title:  "Kodi upgrade"
date:   2024-11-30
tags: [Kodi, Raspberry Pi, DVB]
---

I had issues with some video formats, especially the h265, which showed green palette.

It looked weird, but I found online that it was either a [Kodi](https://kodi.tv/) version or hardware issue.

Since my raspberry pi3 was low on resources and had been my media center for ages, it was time for an update.

## Basic installation

Create the usual Raspberry Pi image with some user and ssh access. This may be done using the image creator, or by adding specific files to the sd card manually.

Kodi is now part of the main debian repos, so we do

```bash
sudo apt install kodi
```

I prefer to create the SD cards with everything on the laptop using [pi-gen](https://github.com/RPi-Distro/pi-gen), but this is a quick installation, and if applicable, I will do the cleanup later.

### No HDMI

Initially, I had no image on the monitor. I find inspiration [here](https://support.thepihut.com/hc/en-us/articles/360008687257-No-HDMI-output-on-my-Raspberry-Pi-4)

Checking connections

```bash
kodi@kodi01:~ $ sudo raspinfo | grep -C 1 -i hdmi
...
Connector 0 (32) HDMI-A-1 (disconnected)
  Encoder 0 (31) TMDS
Connector 1 (42) HDMI-A-2 (disconnected)
  Encoder 1 (41) TMDS
...
```

Seems that the monitors are not detected as connected.

Double checking, and plugging the cable in, solved the issue. Yes, check for dumb errors first - they are easier to fix.

### No kodi

With the monitor now working, but shows only the login text prompt.

Run `kodi` fro the command prompt an it works.

To make it start automatically at reboot, I followed [this](https://raspberrypi.stackexchange.com/questions/111333/how-to-start-kodi-at-startup-automatically)

A line in crontab for the user (not user) was added

```bash
@reboot kodi --standalone
```

### No sound

You must chose the HDMI sound device under `system -> system settings -> Audio`.

### Enable remote control

From the [wiki](https://kodi.wiki/view/Smartphone/tablet_remotes)

![]({{ site.baseurl }}/assets/img/kodi-settings.png)

That worked.

### System time

Look under settings -> region

### Adding sources

You must add video sources to access your movie collection. I smb mount folders from my NAS.

See [the kodi wiki](https://kodi.wiki/view/Updating_or_removing_videos) for some info.

## PVR

I have a PVR hat for my pi. It is something like this [model](https://www.raspberrypi.com/documentation/accessories/tv-hat.html).

### Config tvheadent

We will use tvheadend

```bash
sudo apt-get install tvheadend
```

To configure, connect to `http://<host ip>:9981`. Some installation instruction [here](https://gist.github.com/ProfYaffle/654aa5da1983d651d367)

Going to `config -> dvb input`, we see that the dvb device has been found.

![]({{ site.baseurl }}/assets/img/kodi-tvheadend-dvb.png)

Create a network, **without** predefined muxes and add muxes manually.

To add it manually, you must know the frequencies, for Denmark they are [here](https://digitalt.tv/tv-frekvenser-dvbt-nettet-danmark)

I use the one at Tommerup at 586MHz.

When a network has been created, you must configure the adpater to use that network

![]({{ site.baseurl }}/assets/img/kodi-tvheadend-adapter.png)

As a sidenote, the configuration for tvheadend is at `/var/lib/tvheadend`.

### Config kodi for PVR

In the kodi addons list, there are no tvheadent add-on, and no PVR category.

It must be installed as an os package

```bash
sudo apt-get install kodi-pvr-hts
```

reboot, and you get prompted for it to be activated.

You will need the username and password defined in the tvheadend configuration.
