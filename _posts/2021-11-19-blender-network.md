---
layout: post
title:  "Blender network nodes"
date:   2021-11-21
tags: [Blender, Nvidia, Ansible, Vagrant, Flamenco, Blendnet]
---

We are using blender for videoediting, and are looking into how to speed it up, perhaps by load sharing between multiple machines.

## Network

I started with [this video](https://www.youtube.com/watch?v=FNhUnPWzVaw) which describe a fairly manual way of having local resources to do rendering. Text version is currently unavailable.

and that plugin is out-of-date and doesn't work on newer versions of blender - see e.g. [here](https://cgcookie.com/community/8088-is-there-a-way-to-downlad-the-network-render-addon)



## Flamenco

There is one solution which includes flamenco. It includes a manager, workers and a server. The server part i problematic.

Installing the first two is out-of-the-box. For the server it becomes more difficult, and they suggest to create projects on [cloud.blender.org](https://cloud.blender.org). It is a paid service. I think it makes sense for blender to do stuff like this, while open sourcing their software.

You can install server yourself - I didn't succeed. 

Also, I got confused about the concept of "projects". I just want to dump the rendering somewhere else for our (numerous) standalone videos. There seems to be a world of difference between doing 3D animated movies from scratch in blender, and simply using it for "common" youtube style video editing. The former will require lots and lots of processing power, while the latter is closer to realtime. 

My defunt ansible and other related code is on [gitlab](https://gitlab.com/moozer/blender-flamenco).

Refs

* https://www.flamenco.io/
* https://www.flamenco.io/docs/user_manual/installation/
* https://developer.blender.org/source/flamenco-worker/


## Blendnet

The other suggestion is [Blendnet](https://github.com/state-of-the-art/BlendNet).

I followed the local linux guide (found [here](https://github.com/state-of-the-art/BlendNet/wiki/HOWTO%3A-Setup-provider%3A-Your-own-render-farm-%28local%29---for-Linux-users)) and that worked nicely. Fair warning: you need to known your linux.

In my vagrant setup, I set up a manager and worker node, and started testing.

Installing the addon in blender, adding the manager and the worker was fairly straight forward, and I clicked render.

It took 30s per frame as opposed to 0.1 s per frame on the desktop - so I am currently looking into where the bottleneck is.



So in conclusion, it works. Local resources does not seems to be the focus, and the one-frame-per-task could make sense for 3D rendering, not for "normal" video editing.


BTW, cred to the programmer for forcing the use of HTTPS.

Ref:
* my ansible stuff is on [gitlab](https://gitlab.com/moozer/blender-blendnet)