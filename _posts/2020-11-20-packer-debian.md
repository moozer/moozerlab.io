---
layout: post
title:  Debian packer
date: 2020-11-20
tags: [Packer, Debian]
---

Using packer, I create my default debian image.

The basic idea is to have a base image with the minimal [Debian](https://www.debian.org/) installation, SSH access, known user and so on.

The repo is [here](https://gitlab.com/moozer/packer-debian)

As of today, features are

* Use packer to build .ova files for [vmware workstation](https://www.vmware.com/dk/products/workstation-pro.html). These images should work for [vmware player](https://www.vmware.com/dk/products/workstation-player.html) and [virtualbox](https://www.virtualbox.org/) also.
* Uses [preseed](https://wiki.debian.org/DebianInstaller/Preseed) file for unattended installation
* Serial console enables to look into the installation process. This has the side effect of the resulting image having serial enabled also.
* `make` is used to conditionally build the image.

Features to be implemented:

* [qemu/kvm](https://www.linux-kvm.org/page/Main_Page) images, ie. [qcow2](https://www.linux-kvm.org/page/Qcow2) and [xml for libvirt import](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-domain_commands-creating_a_guest_virtual_machine_from_a_configuration_file).
* user defined in packer and not directly in the preseed file
* saving the installation log
* gitlab-ci mechanics. This is "simple" if yo uhave somewhere to store the resulting images.
