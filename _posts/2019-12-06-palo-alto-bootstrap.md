---
layout: post
title:  Palo Alto boostrap
date:   2019-12-06
tags: [VM, Palo Alto, Bootstrap]
---

We have been working with Palo Alto VMs.

[Bootstapping](https://docs.paloaltonetworks.com/vm-series/9-0/vm-series-deployment/bootstrap-the-vm-series-firewall/prepare-the-bootstrap-package.html) is done using a custom .iso file.

A student made a [project](https://gitlab.com/samuels96/itt3_nwsec_pa-deploy) for that. For the improved version go [here](https://samuels96.gitlab.io/articles/2019-12/Palo-Alto-Firewall-Boostrap-deployment-on-ESXI)

To check the resulting .iso file, I use [furiusiso](https://launchpad.net/furiusisomount). In debian it is `apt-get install furiusiso`.
