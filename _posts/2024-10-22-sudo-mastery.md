---
layout: post
title:  "sudo mastery book review"
date:   2024-10-22
tags: [Book, sudo]
---

Continuing reading from the Lucas bundle of books, I got from the ["Run you own mailserver" kickstarter bundle](https://www.kickstarter.com/projects/mwlucas/run-your-own-mail-server). This time it is ["Sudo mastery, 2nd edition"](https://www.tiltedwindmillpress.com/product/sudo-mastery-2nd-edition/).

Sudo is an interesting tool. I have had my reservation against its use for years, but it is everywhere now and and mixture of convenience and people convincing me has made me adopt it.

The way I have seen it used - almost universally - is as an "su" replacement, except you use the users password not a dedicated root password. Yes, like it is default on ubuntu.
I have also used it for some very specialized tasks, like allowing certain command to be run as root, but passwordless. For example, for [vagrant to use NFS](https://github.com/moozer/ansible-server-kvm/blob/master/templates/sudoers_vagrant.j2).

I found the book interesting and illuminating. It is obvious that Lucas has been doing system administration for a long time, and in places with a diverse crowd of users.

He has a lot of tips on how to allow and limit people, and do it across multiple machines. There is a side comment about the futility of keeping a root password secret, and for that reason alone, `sudo` is worth considering.

Also, you can use `sudo` to allow users to run a program as a different users than root. A use case could be to run something specific as a service account with login disabled.

I kept having the feeling that most of the complex parts might become obsolete, since we are allowing fewer and fewer people command line access to servers. Combining it with something like `ansible` and `AWX`, you can define routine tasks and only allow that for users.

Also, the need for `sudo` in a container probably means that you have a design issue.

Those reservations aside, there will be use cases for more complex sudo setups. Maybe clusters and other large pools of computing power with specialized access with different users could be an example.

As with the other Lucas books, I like his style, and will continue to the next book in bundle.
