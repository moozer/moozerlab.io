---
layout: post
title:  "The book of Kubernetes book review"
date:   2022-11-28
tags: [Kubernetes, Book]
---

At work, we have started working with [`Kubernetes`](https://Kubernetes.io/), and I had the experience of "It doesn't work - now what?". I had drawn a blank on how to extract troubleshooting information. My usual linux ways simply did not reveal anything useful. The conclusion was to up my game and read up on it. It turned out that ["The book of Kubernetes" by Allan Hohn](https://nostarch.com/book-Kubernetes) was part of a [Humble Bundle](https://www.humblebundle.com/) I bought recently.

Kubernetes has a lot of moving parts. A lot of it is confusing for the beginner, and I now suspect that debugging is hard, even for the experience operators.

At its core, `Kubernetes` is just a way of using containers. Keeping it simple, it will just give a [`docker`](https://www.docker.com/)/[`podman`](https://podman.io/) style system, which means that all the tools we have to build and check containers are still usable. It still uses the default [OCI containers](https://opencontainers.org/) with [`containerd`](https://containerd.io/) as the backend. And using `containerd`, [`cri-o`](https://cri-o.io/) can be used to query the lower levels of the Kubernetes stack.

So now `cri-o` is my to-learn list.

Another point I learned is that namespaces is for more prevalent and has more features than I knew. He has dedicated and entire chapter to "Network namespaces".
`Kubernetes` does a lot of smart stuff automatically to handle flow between pods and namespaces, but it all boils down to iptables/[nftables](https://wiki.nftables.org/wiki-nftables/index.php/What_is_nftables%3F#What_is_nftables.3F) rules. `Kubernetes` just generates most of it automatically.

The big selling point of Kubernetes is the load balancing, quotas, limits, autoscaling and such. It works nicely when there are enough resources and multiple nodes. 

I ran into issues related to lack of resources and then only having one node was not a good idea. My hypervisor did dynamic memory scaling, and that made `Kubernetes` reduce the memory usage by killing pods. That combination was unfortunate, and I recommend disabling dynamic memory management in the hypervisor.

All in all `Kubernetes` is BIG, and is hard to start using. I have spend a lot of time learning how to troubleshoot in a linux environment, and that was just not applicable in `Kubernetes`. I miss looking in /var/logs for clues.
I got the feeling that at every new chapter, there was something new that could be misconfigured and hurt me. It didn't help that my friends had comments like "Oh, you run Kubernetes? that means you are vulnerable".

`Kubernetes` seems to be a good solution for large scale container deployments. If you have 5-10 containers, it is way overkill, and might be counter productive.
It also necessitates a continuous pipeline of container updates. It is simply not designed for VM-style package updates. I think many companies would benefit from using LXC or similar to do VM style long lived containers, where you just do "apt-get upgrade" in a cron job to patch.

Getting back to the book, I think is a good resource to get started and a bit further. I like the concrete examples and his very practical approach. It comes recommended, if you want to get started with `Kubernetes`.
