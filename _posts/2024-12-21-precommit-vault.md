---
layout: post
title:  "Pre commit and ansible vault"
date:   2024-12-21
tags: [Ansible, Pre-commit]
---

I have good experiences with `ansible-lint` and similar as pre-commit hooks for git. I use [`pre-commit`](https://pre-commit.com/) to manage it.

In my ansible inventory repo, I have `passwords.yml` that holds - wait for it - passwords. And I want to make a pre-commit hook that ensures that those files are encrypted using ansible vault before commiting them to git.

I found inspiration [here](https://gist.github.com/leucos/a9f42e111a8cfc2ebf6e).

The `check_vaulted.sh` script is in a snippet [here](https://gitlab.com/-/snippets/4785939)

* The script takes a parameter for which vault password file to use
* [Check if the file is vaulted](https://gitlab.com/-/snippets/4785939#L45)
* Check a [pattern for illegal passwords](https://gitlab.com/-/snippets/4785939#L22). You may want to tweak this, or make is parameter.

Save the script to `hooks/check_vaulted.sh` and ensure it is executable `chmod +x hooks/check_vaulted.sh`

Update `.pre-commit-config.yaml` with

```yaml
repos:
  - repo: local
    hooks:
    # ...
    # other hooks go here
    # ...
      - id: check_vaulted
        name: check_vaulted
        entry: hooks/check_vaulted.sh
        args: ['--vault-password-file=vault-file-path']
        language: system
        types: [file]
        files: ".*passwords\.yml$"

```

To test use `pre-commit run --all-files check_vaulted`
