---
layout: post
title:  KVM speed test
date:   2019-12-16
tags: [Libvirt, VM, Benchmarking, Packer, Vagrant]
---

Some time ago I made a recursive speed test for testing nested KVM virtualization.

Using `packer`, we create a vagrant box, runs some speedtest, and spins up a nested VM using vagrant.

The repo is [here](https://gitlab.com/moozer/kvm-speedtest).

(Unimpressive) stats from my test server

| Test            | L0 (host)      | L1             | L2 |
| -------------   |:-------------: | :-----:        | :---: |
| Disk (cached)   | 2771.96 MB/sec | 2825.12 MB/sec | 1833.57 MB/sec |
| Disk (buffered) | 125.66 MB/sec  | 651.48 MB/sec  | 173.55 MB/sec |
| 1GB SHA256      | 15.558 s       | 16.9574 s      | 31.5948 s     |

Something is odd with the buffered disk read on L0 and L3 is not included, since I got a vagrant timeout. On this hardware, it runs virtualmachines nicely, but it is not good for nested virtualization. I have seen hardware that handles multiple nesting much better - I think it is CPU+chipset related.

And when doing nesting, one should consider using network storage.
