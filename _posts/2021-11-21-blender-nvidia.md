---
layout: post
title:  "Blender using Nvidia GPU"
date:   2021-11-21
tags: [Blender, GPU, Nvidia]
---

I have a vagrant machine with a Geforce 1060 passthrough. We will use it to test if there is any reason to dig deeper into using GPUs for optimizing speed.

It is a known good idea, if you are doing 3D graphics, but we are doing video editing, and I am unsure if GPUs will contribute anything to that.

I wanted to test with real hardware, but the laptop I had in stock had an old Radeon HD 7870M, which is a [GCN 1st generation architecture](https://en.wikipedia.org/wiki/List_of_AMD_graphics_processing_units#Radeon_7000_series) and not supported by Blender.


## Installing CUDA drivers

We follow the [debian wiki guide](https://wiki.debian.org/NvidiaGraphicsDrivers)

1. add debian non-free and contrib to sources.list
2. apt-get install nvidia-detect

Running nvidia-detect suggests installing nvidia-driver
3. install nvidia drivers and cuda
4. fix xorg issue
4. reboot (due to nouveau driver)

The complete vagrant+ansible setup is available on [my gitlab](https://gitlab.com/moozer/blender-nvidia)

## Initial speed test

I have a 1000 frames of simple video editing, and on the CPU this takes around 2 minuttes (at 60-150 ms per frame)


## Speed test with gpu

Enable GPU usage by going through the menus: "Edit -> Preferences -> System -> Cycles Render Devices".

Running the rendering again, we get no change. This is not surprising since we do not use path tracing or any of the features that graphic cards are good for.

I had hoped that it might be relevant for the encoding of the final video file, but no.

## Note on Nvidia graphics card and passthrough

You must hid KVM from the drivers, otherwise they will be present, but not functioning. See [the vagrant-libvirt docs](https://vagrant-libvirt.github.io/vagrant-libvirt/configuration.html#pci-device-passthrough)