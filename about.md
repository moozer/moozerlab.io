---
layout: post
title: About
permalink: /about/
---

This site is where I do my notetaking so I can find stuff again, and to make it easy to share. I also have a blog on [wordpress.com](https://moozing.wordpress.com) which I use for longer detailed blog entries - this site is for notes and smaller guides.

The website content is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

I try to credit people, organisations and so on using names and links whenever I use other peoples work. If I have omitted something, either drop me a DM on twitter or send a pull request with corrections.


Site technicalities
---------

This site is a statically generated site on [Gitlab](https://gitlab.com)

It uses

* [Jekyll](https://jekyllrb.com/)
* [Flexible-Jekyll](https://github.com/artemsheludko/flexible-jekyll) Jekyll theme

The stuff above is GPL'ed in some version, and I have made simple modifications to the theme to fit my needs. Full code is on [gitlab](https://gitlab.com/moozer/moozer.gitlab.io/).
