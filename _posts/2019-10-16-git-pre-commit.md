---
layout: post
title:  "git pre-commit"
date:   2019-10-16
updates: 2020-07-01
tags: [Git, Ansible]
---

In my `ansible` inventory I have files with passwords them, and I use `ansible-vault` to protect it.

The issue is when I accidentally commit a non-vaulted file.

The solution is to add [pre-commit hooks](https://git-scm.com/docs/githooks) in git. There are two solutions; one is to have a separate repo for hooks, and the other is to have is a part of the repo to use pre-commit. We go for the second solution.

In this example, we use the directory `.git-hooks` under version control. The dvelopper will need to issue `git config core.hooksPath .git-hooks` or create symlinks after cloning.

Quick guide
--------------

Assumptions
* We only look at password files
* Password files are called `passwords.yml`
* Vault secret is located in a file called `vault-password`.

    Which is either in `.gitignore` file or in a different directory, so we don't accidentally add it to the repo.

The process

1. Create a file called `.git-hooks/pre-commit`, with the following content

{% include gitlab-snippet.html snippet_id = 1991891 %}

2. make it executable: `chmod +x .git-hooks/pre-commit`.

3. set the repo to use hooks from the new location: `git config core.hooksPath .git-hooks`


Updates 2020-07-01: pre-commit hook updates for speed.
