---
layout: post
title:  "Gitlab CI locales and encodings"
date:   2019-12-30
tags: [Gitlab, Gitlab-CI, Python, Docker]
---


When running stuff using gilab CI, sometimes your run into issues related to different environments.

## Reading file and unicode

Some time ago I had an issue about python, unicode and docker. Go [here](https://gitlab.com/moozer/unicode-test/blob/master/readme.md) for result and links.


## Bad local running `pip install`

This time it is related to installing `pip` packages

I do a generic install of packages

```
$ pip3 install -r requirements.txt
...
Collecting urwidtrees (from -r requirements.txt (line 2))
...
  locale.Error: unsupported locale setting
```

I use my own generated docker instance at `registry.gitlab.com/moozer/docker-deb-pip:latest`.

Adding explicit locales work.

```
before_script:
- export export LC_ALL="C"
- pip3 install -r requirements.txt
```
