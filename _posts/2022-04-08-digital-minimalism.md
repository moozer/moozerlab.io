---
layout: post
title:  "Digital minimalism book review"
date:   2022-04-08
tags: [Book]
---

I recently read the book ["Digital minimalism" by Cal Newport](https://www.calnewport.com/books/digital-minimalism/). It was the danish translation published in 2020.

The book is a call out against the overuse of social media, and how it affect us, both personally and on a more societal level. It is a sort of self-help book to counter the growing addiction many people have to social media and to their digital devices.

Through examples and references to research, he builds the picture of how the tech giants manipulate our lives toward using their platform and spend our attention on them. He compares them to the tobacco industry, and their tactics on how to not get banned and to keep people smoking.

One of his main points is that we are to use the technologies, where they make sense, and give value to us. E.g. checking SoMe every other moment yields very little return, but checking a couple of time per week, could be a great way of keeping in touch with people. Attention economy is trying to convince us, that we are missing out, when in reality we are not, and it is basically a wasted time. A comment he makes is that you should never click the "like" button - it feeds the algorithms and incentives the sender to write more on SoMe.

He also emphasizes the importance of being alone. In this context `alone` means to not be stimulated from the outside. You may be `alone` at a café where you sit and think for yourself without being interrupted. Using SoMe to excess will rob you of that free time, and you will not have the quiet time needed to be creative.

Another one of this suggestions is to build something. Physically, that is. Somehow this feels intuitively true, that we are made to use of hands, and too much time in front of screens is bad. Whether `building` is renovating a house, painting, or something else entirely doesn't matter as long as you do something in the physical world.

I really liked this book. It echoes some of the thing I have concluded myself multiple times. Combining with concepts like the price of `context switching`, it paints a picture of our digital devices as counter productive for what most people want to achieve with their lives. I see it as we are practicing being interrupted by unimportant events, and that prevents us from delving deep into a topic and to be in a state of flow. Multitasking is only for trivial tasks, and you probably know it.




