---
layout: post
title:  "Allsky setup"
date:   2025-01-05
tags: [Allsky, Raspberry Pi]
---


Spoiler; it worked

![Example of the sky from my back yard]({{ site.baseurl }}/assets/img/allsky_250104_example.png)

We want to build something like this [one](https://hackaday.io/project/175041-cheap-all-sky-camera-project-with-raspberry-pi) and [this](https://www.youtube.com/watch?v=GuDTKsG9rX4)

The allsky software is readily available on [github](https://github.com/AllskyTeam/allsky) and the associated instructables is [here](
https://www.instructables.com/Wireless-All-Sky-Camera/)

All in all it seems to be very doable.

## Selecting camera

I tried with a good USB webcam, but that is not supported. Only the more expensive [ZWO cameras](https://www.zwoastro.com/shop/) or the Raspberry Pi cameras. See the list [here](https://github.com/AllskyTeam/allsky?tab=readme-ov-file#requirements)

In the end, I bought a [raspberry pi HQ camera](https://www.berrybase.de/en/raspberry-pi-high-quality-kamera-m12-mount) and the associated [fisheye lens](https://www.berrybase.de/en/2.7mm-weitwinkelobjektiv-m12-mount).

As can be seen from the image above, it works way good enough for my purposes.

## Installing allsky

On an default installed [RaspiOS](https://www.raspberrypi.com/software/operating-systems/), I set up users, wifi and such as per normal installation. I chose the `Lite` version.

For reference I use a Raspberry Pi 3, 1GB RAM version

```bash
$ cat /proc/cpuinfo | grep Model
Model       : Raspberry Pi 3 Model B Plus Rev 1.3
```

`Allsky` is pulled directly from git repo as per the [documentation](https://htmlpreview.github.io/?https://raw.githubusercontent.com/thomasjacquin/allsky/master/html/documentation/installations/Allsky.html)

Git will be needed

```bash
apt-get install git
```

Connect camera and install software

```bash
git clone --recursive https://github.com/AllskyTeam/allsky.git --depth 1
cd allsky
./install.sh
```

post installation

1. go to the website
2. login using default user and pass is `admin:secret`
3. After clicking ok to the config in the web UI, it works.

## Setup and testing

The first setup

![Prototype of my allsky setup]({{ site.baseurl }}/assets/img/allsky_system_proto.png){:width="25%"}

My raspberry 3 does not have enough resources to create the timelapse videos, so for now it is disabled. It is an issue about not enough RAM for the `ffmpeg` command. We must do some sort of off-device image processing.

Otherwise setting focus was the most hassle. It takes and image every 90 seconds, and I was outside in the cold setting focus. Do this using some video software, unless you like freezing. Also, power banks work poorly in the cold, and you get the best images on cold, frosty, cloudless nights - so plan for a power supply.

## Ideas and future work

Next on the list is how to handle generating videos in a VM or similar, and the actual physical mounting and cabling of the system.

`Allsky` also supports "remote website". It seems to mean that I can use the raspberry as a sensor, and then access the image somewhere else. I need that anyway since backup is a thing.

There is a [network of people sharing the allsky videos, live views, and such](https://www.thomasjacquin.com/allsky-map/). This is cool. I have my usual privacy reservations, since I think it is fairly simple to go from video of the night sky to the specific camera coordinates.

I noticed [this one](http://mcwaimea.tplinkdns.com:3000/allsky/) on Hawaii where they overlay star constellations. I want that also.

### Notes

* I also tested [`AstroBerry`](https://astroberry.io/). It seems to be a very complete astronomical distribution for Raspberry. Not what I needed for this project.
