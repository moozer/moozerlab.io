---
layout: post
title:  "Ansible new project"
date:   2020-11-28
tags: [Ansible, Gitlab-CI, Ansible roles]
---

This is my checklist of what to include when starting a new project.

This is a rought outline, and it should be put in a `.tar.gz` for quick startup.

1. Create the inventory file

    e.g. `echo newserver >> inventory`

2. Create the `ansible.cfg` file.

    Content is something like this

    ```
    [defaults]
    inventory = inventory
    roles_path = ./roles-ext:./roles
    pipelining = True
    ```

3. Create a `role_req.yml` for dependencies

    Content is comething like

    ```
    #- src: git+https://gitlab.com/<somerepo>
    #  name: server-someserver
    ```

    See [galaxy docs](https://galaxy.ansible.com/docs/using/installing.html#installing-multiple-roles-from-a-file) for syntax

    I intentionaly do not call it `requirements.yml` to avoid any python confusion.

3. Create the roles directory for external roles `mkdir roles_ext`

4. Fetch roles using `ansible-galaxy install -r role_req.yml -p roles-ext` (if you added any)

6. Add an initial generic `playbook.yml`

   Content is comething like

   ```
   - hosts: all
     become: true

     task:
     - name: say hi
       debug: msg="hi"
   ```

6. Spin up server, reset passwords and such

5. Add group vars and host_vars `passwords file`

    I follow [my own suggestion]{% post_url 2019-09-24-ansible-user %}.

7. Ensure you have ssh access to the server `ssh user@newserver`

8. Run the playbook `ansible-playbook playbook.yml`

    check that it works

9. Add the gitlab-ci part

    See [the post about that]{% post_url 2019-07-15-Ansible %}.

9. Do `git init`

10. create `.gitignore` with the following entries

    ```
    vault-password
    roles_ext
    ```
11. Create a vault password file and put it in `vault-password`

12. Encrypt the password files `ansible-vault encrypt --vault-password-file vault-password host_vars/*/passwords.yml`

13. Add the lot to git `git add .` and `git commit -m "first commit"`
