---
layout: post
title:  "Measuring electric energy"
date:   2021-11-27
tags: [Energy, Modbus, Bornhack, Grafana]
---

One of our POPs at [Bornack](https://bornhack.dk) is *very* far from the main outlet, and we have seen low voltages. We want some measurement device that we can put with the POP to assess the issue, and show some graphs.

## Energy measurement

This is very *in*, these days and I got a [Carlo Gavazzi em340](https://www.farnell.com/datasheets/2166985.pdf). These costs around €150.

It can measure the potential, current, power and frequency. This may be read on the display. The model I have uses modbus, sp we need some sort of modbus devices to extract the data.

## Basic design

We have some subpart to the project.

![Block diagram of measurement system](/assets/img/potmeasurements.png){: width="75%" }

* "Some box": Since we doing 230V, then we need to take casing seriously, so no copper is exposed. We also need some sort of environmental protectection, since the box is going to lie on a field.
* "EM340": The measurement device. This one is designed to be a secondary power measurement device for tenants, machinery and such. It is rated to be used for billing.
* "Raspberry": This is a minicomputer that I have in stock. It does not support modbus, so we qill add a "hat" to it.
* "5V PSU": The raspberry needs 5V, so we need a power supply. I would like it to also act as a UPS, but have not found anything relevant yet.
* "modbus module": This is the modbus hat.
* "Serial->scrapable": Somehow we need to move the data from modbus to a format the prometheus can consume. Perhaps [modbus_exporter](https://github.com/RichiH/modbus_exporter) is relevant.


## Current state

I have hooked it up with one phase in a nice box to check that the EM340 works. For some reason I had an appropriate box in stock.

![Picture of the EM340 in a box](/assets/img/potmeasurements-box.jpeg){: width="75%" }


