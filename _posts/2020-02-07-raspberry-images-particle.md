---
layout: post
title:  "Raspberry and particle"
date:   2020-02-07
tags: [Raspberry Pi, Particle.io]
---

I have made a system for generating rasperry pi images using gitlab ci. It is available [here](https://gitlab.com/moozer/raspberry-image-static). It includes a [simple website](https://moozer.gitlab.io/raspberry-image-static) to give easy access to the generated images.

Particle.io
------------

For a project we want to use boron devices from [particle.io](https://particle.io). For that I created a raspberry pi image project. The repo is [here](https://gitlab.com/moozer/raspberry-particle) and the website is [here](https://moozer.gitlab.io/raspberry-particle)

It is currently not working due to ARMv6, v7 and v8 issues, and that the CI buid failed due to some QEMU thing when installing npm.

I will get back to this at a later time.
