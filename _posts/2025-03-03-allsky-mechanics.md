---
layout: post
title:  "Allsky mechanics"
date:   2025-03-03
tags: [Allsky, Raspberry Pi, Openscad]
---


We have the electronics, and now we will set up the [allsky](https://github.com/AllskyTeam/allsky?tab=readme-ov-file#allsky-camera--) system.

## Parts

The plumbing parts and the electronics was trivial. And installing Allsky was easy too (see other post about that)

I create a plate for the camera to be attached to, and some plate for the raspberry with casing.
![View of raspberry pi setup connected to drainage pipe lid](../assets/img/allsky/allsky-internals.png))

Finding a 4" dome was difficult. There were a lot, but none shipped to my place. I ended up with a dome from a fake security camera and made a ring to attach it.

![View of the dome covering the camera](../assets/img/allsky/allsky-dome.png))

The casing need to protect the electronics from rain coming from above, so the dome and the attachment ring had to be water tight. I used hot glue.

I decided against a completely closed box. The drainage pipe used as the main "box" is also open at the bottom. My expectation is that condensing water on the inside will not be an issue, since the pi is powered and we will have minimal continuous heating of the system.

We'll see if that is a decision that needs revising at some point.

## Setup

Find a good spot with Wifi and power.

![Allsky system mounted on a drying line outdoors](../assets/img/allsky/allsky-mounted.png))

As seen, it is mounted on the poles for the drying lines. Come summer, we'll see if they move too much when we start using them for wet clothes again. It was a very convenient place with wifi and 2m up so people will not show up on the images.

I think we must find a nice stand-alone pole for the permanent installation. Power is also a temporary thing. This is the first location, and we will get some experience with the allsky, and then evaluate what need to be changed.

And the usb cable got in the way. So for "iteration 2", we either need to find an angled usb connector, or remove the back rpi casing.

## Result

Well... after installing it died 8 hours later.

I should have taken the "undervoltage detected" errors in the logs more seriously. Did I mention an iteration 2?

Also, the dome is simply not clear enough

![Blurry image of the night sky from the current allsky setup](../assets/img/allsky/allsky-nightsky.png))

Comparing it to what the camera gives me without the dome (see other blog post), it is not good enough. We will keep it for now, untill I get a new dome and make the relevant parts.
