---
layout: post
title:  "Ansible users"
date:   2019-09-24
tags: Ansible
---

My default way of handling users are described below. We don't use sudo, and mostly use [Debian](https://debian.org) for servers and workstations.

In `group_vars/all/all.yml`, we keep the default values

{% raw %}
```
# default system users
user_name: sysuser
user_pub_key_file: "{{ inventory_dir }}/pub_keys/{{ user_name }}_key.pub"
root_user: root

ansible_user: "{{ user_name }}"
ansible_become_pass: "{{ root_pass }}"
ansible_become_user: "{{ root_user }}"
ansible_become_method: "su"
```
{% endraw %}

For each host we have `host_vars/<hostname>/passwords.yml`

```
root_pass: ThePasswordForTheRootUser
user_pass: ThePasswordForTheAdmUser
```

Remember to put this file in the [vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html).

and in `public_keys` (in parallel with `host_vars` and `group_vars`), there is a file called e.g. `sysuser_key.pub` containing the public key to be associated to the non-privileged system user.
