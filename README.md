moozer.gitlab.io
==============================

Personal site for moozer

Building in CI
----------------------------------

CI should handle most things.
Look in [.gitlab-ci.yml](.gitlab-ci.yml)

Manual build
------------------------------------

Local build could be used for debugging and testing.

1. Install bundle: `sudo apt install ruby-bundler ruby-dev`
2. Install gems

    ```bash
    bundle config set --local path '.gems'
    bundle install
    ```

Local testing: `bundle exec jekyll serve`

Local build and link check

```bash
bundle exec jekyll build -d test
bundle exec htmlproofer --ignore-url '#' --ignore-status-code 503,403 --log-level :debug  --typhoeus '{"headers":{"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0"}}' ./test
```

We could also use `--ignore-url` for specific sites. For the current way of testing links, see [.gitlab-ci.yml](.gitlab-ci.yml)
