---
layout: post
title:  "Bootstrap ansible roles with vagrant test"
date:   2020-12-04
tags: [Ansible, Ansible roles, Vagrant]
---

I want a nice and easy way of creating new roles, with all the tests and gitlab CI stuff.

`Ansible-galaxy init` handles the generic structure and then we added vagrant based tess and gitlab ci.

The repo is [here](https://gitlab.com/moozer/ansible-role-bootstrap). I suggest cloning from it - not forking - since you mostlikely want multiple roles.
