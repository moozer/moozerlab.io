---
layout: post
title:  "Drawing with a wacom tablet"
date:   2021-11-17
tags: [Wacom, Drawing]
---

I make schematics and simple draings using my wacom tablet.

As software, I currently use [Krita](https://krita.org/en/).

And as drawing tool, I have my trusted [Wacom Intuos Pen & Touch tablet (model CTH-690)](https://101.wacom.com/UserHelp/en/TOC/CTH-690.html). It worls out-of-the-box on my Debian systems.

# Multiscreen

It is a 16:9 tablet and that ratio fit one screen, so in my two-monitor setup, I has to be limited to one monitor only.

It is a [common](https://feldspaten.org/2017/05/06/ubuntu-linux-map-wacom-to-one-screen-when-using-multiple-screens/) [problem](https://unix.stackexchange.com/questions/428747/restrict-graphic-tablet-to-primary-display).

The process:

1. Find the monitor to limit to: `xrandr` ("HDMI-1 in my case)
2. Find the wacom controls to map: `xinput | grep -i Wacom` (Ids 16, 17 and 18 in my case)

    Note that you might need to install `xinput`

3. Do the mapping: `for ID in $IDS; do xinput map-to-output $ID $MONITOR; done`
    
    For my setup: `for ID in 16 17 18; do xinput map-to-input $ID HDMI-1; done`

    Please note that the ID values may change.

4. Redo after reboot.
