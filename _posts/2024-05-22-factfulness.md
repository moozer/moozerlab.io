---
layout: post
title:  "Factfulness book review"
date:   2023-05-23
tags: [Book]
---


I recently read [Factfulness](https://en.wikipedia.org/wiki/Factfulness) by Hans Rosling. It was recommended by a colleagues, who thought I needed cheering up.

It turned out that it was a book I had already read some years ago. In any case, it was well worth a revisit.

Rosling (and his family) have worked for years to bring fact and data into the conversation about almost anything related to public health and economics. And in doing so he has identified 10 traits about human that warp our world view for the worse.

![The ten drama instincts]({{ site.baseurl }}/assets/img/gapminder-10-instincts.png){: width="75%" }

(from [gapminder](https://www.gapminder.org/factfulness/))

Going through the list, they are very recognizable. I fell it is one of those cases where you have this diffuse sentiment, and someone brings order.

They divide the world into income levels from level 1 (at less than $2 per day) to level 4 (at more then $32 per day). This is an interesting case, and they have been collecting images on how people live on the different levels. It turns out that people live the same way, no matter where they live. Housing, water, education, food, and so on is prioritized more when you get richer. This is not really surprising, but to me it was mostly a reminder that we are less different than most people instinctively think (see also the `gap` instinct).

[An example of Level 4](https://www.gapminder.org/dollar-street/families/family-440). This is not me, but close. [And an example of level 1](https://www.gapminder.org/dollar-street/families/family-169). Maybe I have never met someone on level 1. In any case it puts our daily "struggles" in perspective.

On a different topic, I have generally been blaming social media and the media in general for the polarization of debates and politics. It is just difficult to have a debate when everything is one sentence, a meme or a short sound bite.

He addresses this too. This is a combination of a lot of the instinct he describes in the book. A simple story about the others doing something we can fear (garnished with some sensationalism) is easy to sell.

Also, "Good news is not news". This is in part because most good things is either a non-event or happening to slowly, while bad things like accidents, volcanoes, wars, and such are more flashy and sudden.

Yet another takeaway I have from the book is the part about noticing stuff working or not. I have been comparing engineers with sales people, and concluded that engineers focus of the problem at hand, not what works, while sales people must focus on what works and not the problem. This is probably not true.

According to Rosling, we all focus on the bad stuff (see `negativity` instinct). His suggestion is to look at the data and see that things are improving. This could be a year, a decade or a century. This is a neat trick that will help detect slow progress.

Perhaps I should reread the book (again) in a couple years. Or maybe make it mandatory reading for the kids.

## Other refs

* [Gapminder](https://www.gapminder.org/)
* [Money street](https://www.gapminder.org/dollar-street?topic=play-areas&active=5d4beacccf0b3a0f3f34b1c2)
