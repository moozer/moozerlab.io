---
layout: post
title:  "Blender speed optimizing"
date:   2021-11-21
tags: [Blender, Blendnet]
---

I am currently looking into blender and how to distribute load. 

See [here]({% post_url 2021-11-19-blender-network %}) for an introduction.

It takes 30'ish seconds per frame...

I have moved it away from a vagrant VM setup, and it now runs on actual hardware - no change.

## looking in the logs

I pipe stdout to a log. The connection info remains.

### block I - init

``` 
>err>> INFO: Starting render process
>std>> Fra:7 Mem:173.08M (0.00M, Peak 173.08M) | Time:00:00.00 | Mem:0.00M, Peak:0.00M | Scene, View Layer | Initializing
...
>std>> Fra:7 Mem:173.03M (0.00M, Peak 173.08M) | Time:00:00.00 | Mem:0.00M, Peak:0.00M | Scene, View Layer | Updating Shaders
>std>> Fra:7 Mem:173.53M (0.00M, Peak 173.53M) | Time:00:00.04 | Mem:0.00M, Peak:0.00M | Scene, View Layer | Updating Background
...
>std>> Fra:7 Mem:174.81M (0.00M, Peak 175.06M) | Time:00:00.04 | Mem:1.53M, Peak:1.53M | Scene, View Layer | Updating Device | Writing constant memory
```
Perhaps we can removed "Updating Shaders" - wouldn't give much.


### block II Path tracing

```
>std>> Fra:7 Mem:174.81M (0.00M, Peak 175.06M) | Time:00:00.04 | Mem:1.53M, Peak:1.53M | Scene, View Layer | Path Tracing Sample 1/128
>std>> Fra:7 Mem:554.54M (0.00M, Peak 554.94M) | Time:00:00.28 | Remaining:00:30.63 | Mem:381.22M, Peak:381.23M | Scene, View Layer | Path Tracing Sample 2/128
>std>> Fra:7 Mem:586.20M (0.00M, Peak 594.13M) | Time:00:00.41 | Remaining:00:23.59 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 3/128
>err>> INFO: Command "savePreview" completed
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:00.52 | Remaining:00:20.03 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 4/128
...
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:05.36 | Remaining:00:08.96 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 49/128
>err>> INFO: Command "savePreview" completed
INFO: Removing blob "fe1d111ba943d8d53a25483cbf7c728393761af2"
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:05.62 | Remaining:00:09.10 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 50/128
...
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:10.38 | Remaining:00:04.00 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 94/128
>err>> INFO: Command "savePreview" completed
INFO: Removing blob "7b8f80dd027964c37796f8259d98a0349bdad48d"
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:10.53 | Remaining:00:03.90 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 95/128
...
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:14.31 | Remaining:00:00.22 | Mem:381.22M, Peak:381.24M | Scene, View Layer | Path Tracing Sample 128/128
>std>> Fra:7 Mem:554.55M (0.00M, Peak 594.13M) | Time:00:26.49 | Mem:381.22M, Peak:385.78M | Scene, View Layer | Finished
>std>> Fra:7 Mem:173.22M (0.00M, Peak 594.13M) | Time:00:26.49 | Sce: Scene Ve:0 Fa:0 La:0
>std>> Saved: '/home/sysuser/worker/BlendNet_cache/ws/tmptq1z1ifmfokusridning1-2111202343-7-Zpi_0/_render.exr'
>std>>  Time: 00:26.84 (Saving: 00:00.34)
```  

"Path tracing" takes most of the time. 

## Conclusion

This is not a viable solution. If we want a frame time to in the 10-20ms range, we need to find a way of speeding things up by x1000. This is not doable with Blendnet and doing tasks frame-by-frame. 

Perhaps, massive tweaking of blender parameters or GPUs. 