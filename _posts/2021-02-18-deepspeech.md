---
layout: post
title:  "Offline subtitles experiment"
date:   2021-02-19
tags: [Subtitles, Vagrant, Deepspeech]
---

## Speech-to-text

I am working on online courses, and subtitles are a good idea, so I wondered how well it worked.

After some searching, many suggests using the google api for that and a lot of paid services. E.g. [Pytranssciber](https://github.com/raryelcostasouza/pyTranscriber) uses google, and looking at the google pricing model [you get 1h for free per month](https://cloud.google.com/speech-to-text/pricing#pricing_table), which is not enough for my use.

There are some opensource software like [deepspeech](https://deepspeech.readthedocs.io/en/v0.9.3/?badge=latest) and wav2letter. The latter is a facebook project and "it has been moved to [flashlight](https://github.com/facebookresearch/flashlight)". Also [CMUSphinx](https://cmusphinx.github.io/wiki/tutorial/) (and [pocketsphinx](https://pypi.org/project/pocketsphinx/)) could be relevant.

Since I want subtitles not just the text, I found [AutoSub](https://github.com/abhirooptalasila/AutoSub) which uses deepspeech.

## Testing it

I have mad a gitlab project with the Vagrantfile to get started. See the readme file for details.

{% include youtube.html id='1SE0w4un23Q' %}

There is a dependency on the vbguest plugin, this was needed to get two-way sync running using Virtualbox. Also, I needed to use `contrib-buster/debian64` basebox which includes the vboxfs.
On my dev machine, I use KVM+NFS, so there two-way "sync" was already working (see [a previous post]({% post_url 2020-12-09-sudo %}) about that.

The repo is on [gitlab](https://gitlab.com/moozer/vagrant-deepspeech).

If the Vagrant part is interesting, I have made a "getting started" [free course](https://courses.mnworks.dk/p/introducing-vagrant-for-vm-sharing) about it.