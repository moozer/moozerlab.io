---
layout: post
title:  "Deploy jekyll on gitlab pages"
date:   2019-09-24
tags: [Gitlab-pages, Gitlab-CI, Jekyll]
---

I use on of two version.

`BASEURL` is the extra path also defined in `_config.yml`. In gitlab pages it is normally the project name.

# Deploy w/o linkchecking

In `.gitlab-ci.yml`, test and deploy looks like this

```
pages:
  stage: deploy
  before_script:
  - bundle install
  script:
  - bundle exec jekyll build -d ./public
  artifacts:
    paths:
    - public
  only:
  - master

```

# Deploy w. linkchecking

In `.gitlab-ci.yml`, test and deploy looks like this

```
variables:
  BASEURL: myproject

test:
  stage: test
  before_script:
  - bundle install
  script:
  - bundle exec jekyll build -d ./test/$BASEURL
  - bundle exec htmlproofer --assume-extension --url-ignore '#' --log-level :debug ./test
  artifacts:
    paths:
    - test

pages:
  stage: deploy
  script:
  - cp ./test/$BASEURL public
  artifacts:
    paths:
    - public
  only:
  - master

```

The snippet deploy pages after test links. See [linkchecker]({% post_url 2019-09-25-linkchecker %}) for more info.
