---
layout: post
title:  "ERR_UNSAFE_PORT"
date:   2024-10-29
tags: [Chromium, Firefox]
---

I did the usual ssh portforward

```bash
ssh someserver -L 10080:127.0.0.1:5000
```

Connecting using Brave gave me this interesting error

![]({{ site.baseurl }}/assets/img/err_unsafe_port_screenshot.png)

Firefox does the same.

**ERR_UNSAFE_PORT** means that you are trying to connect a port (`10080`in this case) that is on the built-in list of ports that the browser will not serve. This is a security feature to avoid malicious javascript connecting directly to vulnerable services.

The list of ports is in the code [here](https://searchfox.org/mozilla-central/source/netwerk/base/nsIOService.cpp#115). If you must, you can [whitelist a port](https://www.partitionwizard.com/partitionmanager/err-unsafe-port.html). In Firefox it is in [about:config](about:config). For chrome, you can use the `--explicitly-allowed-ports` command line parameter.

The easy solution is to use a different port.
