---
layout: post
title:  "git package not updating"
date:   2024-10-18
tags: [apt]
---

`debsecan` told me I have a security issue with the installed git. This turned out to be not git related, but a misconfigured apt.

It is [this one](https://security-tracker.debian.org/tracker/CVE-2023-25652) and there is a security update available

And I had expected that the available security patch would just be installed by [unatteded-upgrades](https://wiki.debian.org/UnattendedUpgrades).

Looking at the policy

```bash
➜ apt-cache policy git
git:
  Installed: 1:2.39.2-1.1
  Candidate: 1:2.39.2-1.1
  Version table:
     1:2.39.5-0+deb12u1 500
        500 http://security.debian.org/debian-security bookworm-security/main amd64 Packages
 *** 1:2.39.2-1.1 990
        990 http://cdn.debian.net/debian bookworm/main amd64 Packages
        100 /var/lib/dpkg/status
     1:2.30.2-1+deb11u3 500
        500 http://security.debian.org/debian-security bullseye-security/main amd64 Packages
     1:2.30.2-1+deb11u2 500
        500 http://cdn.debian.net/debian bullseye/main amd64 Packages

```

We see that the current version is priorities at level 990 and the version I want is at 500. For more on priorities see e.g. [Debian administrators handbook](https://debian-handbook.info/browse/en-US/stable/sect.apt-get.html#sect.apt.priorities).

I have a misconfiguration related to security updates for `bookworm`.

In apt config

```bash
➜ cat /etc/apt/apt.conf
...
APT::Default-Release "bookworm";
...
```

It must be changed to

```bash
➜ cat /etc/apt/apt.conf
...
APT::Default-Release "/^bookworm/";
...
```

There is even a warning about it in [the debian docs](https://wiki.debian.org/AptConfiguration#Be_careful_with_APT::Default-Release).

and after an apt-get update we get the result we want.

```bash
➜ apt-cache policy git            
git:
  Installed: 1:2.39.2-1.1
  Candidate: 1:2.39.5-0+deb12u1
  Version table:
     1:2.39.5-0+deb12u1 990
        990 http://security.debian.org/debian-security bookworm-security/main amd64 Packages
 *** 1:2.39.2-1.1 990
        990 http://cdn.debian.net/debian bookworm/main amd64 Packages
        100 /var/lib/dpkg/status
     1:2.30.2-1+deb11u3 500
        500 http://security.debian.org/debian-security bullseye-security/main amd64 Packages
     1:2.30.2-1+deb11u2 500
        500 http://cdn.debian.net/debian bullseye/main amd64 Packages
```

`git` will now get security updates.

And `apt-get upgrade` revealed other packages with the same issue.

I have fixed this before - time to check my ansible scripts.
