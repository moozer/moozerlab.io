---
layout: post
title:  "GPS antenna wall mount"
date:   2025-01-02
tags: [GPS, 3D print, Openscad]
---


I made a mount for my gps antenna that I can screw into the wall. The design was made using [Openscad](https://openscad.org)

![GPS antenna in mount on wall]({{ site.baseurl }}/assets/img/gps-mount-final.png)

Looks very much like this model a [Boat GPS Navigation External Antenna (5m Cable)](https://www.amazon.in/Navigation-External-Compatible-Matsutec-Receiver/dp/B096PLQTTP)

![White dome shaped marine GPS antenna]({{ site.baseurl }}/assets/img/gps-mount-antenna.png){:width="25%"}

At first glance it seemed that I just need to make [openscad](https://openscad.org/) make an M26 screw. Actually, it wouldn't help since it it not a standard screw size (M24 and M27 exist), so we would still need to custom define it.

Googling yields [this](https://digitalyacht.co.uk/product-category/ais-systems/accessories/antenna-bases/) which seems correct. So we will go for 1 inch and 14 TPI, ie. pitch 1.814 mm

A bit More googling tells me that this is standard for marine antennas.

## Screw

I initially looked at [boltsparts](https://github.com/boltsparts/boltsparts), but failed at defining my own screw.

[BOSL2](https://github.com/BelfrySCAD/BOSL2) worked out better for me.

Download [latest](https://github.com/BelfrySCAD/BOSL2/archive/refs/heads/master.zip) and extract to `$HOME/.local/share/OpenSCAD/libraries` (cf [openscad wiki](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries))

Note that `file -> show library content...` will reveal where Openscad imports from.

You can set all screw parameter as you wish. See [the wiki for details](https://github.com/BelfrySCAD/BOSL2/wiki/screws.scad#section-screw-and-nut-parameters)

![White dome shaped marine GPS antenna]({{ site.baseurl }}/assets/img/gps-mount-screw.png)

The connector screw was made like this

```openscad
include <BOSL2/std.scad>
include <BOSL2/screws.scad>

spec = [["system","ISO"],
        ["type","screw_info"],
        ["pitch", 1.814],
        ["head", "hex"],
        ["head_size", 36],
        ["head_height", 10],
        ["diameter",26],
        ["thread_len", 25],
       ["length",35]];
screw(spec,tolerance=0);
```

## The wall mount

The final design looked like this

![White dome shaped marine GPS antenna]({{ site.baseurl }}/assets/img/gps-mount-mount.png)

The issues I had with the screw came in handy with the mount, since I could just use the same library to add M4 screw holes.

```openscad
module mount_plate() {
    translate( [0, mount_arm_w/2, -mount_arm_l/4] ) {
        difference() {
            // actual back plate
            hull() {
                ...
            }

            // two holes for screws
            translate( [mount_arm_h/2, 0, 0] )
                rotate( [0, 90, 0] )
                    screw_hole("M4,40",head="flat",counterbore=0,anchor=TOP);
                
            translate( [mount_arm_h/2, 0, mount_arm_l] )
                rotate( [0, 90, 0] )
                    screw_hole("M4,40",head="flat",counterbore=0,anchor=TOP);
        }
    }
}
```

I also added a slit for the antenna cable along with a guide for a cable tie.

Files are available on [thingiverse](https://www.thingiverse.com/thing:6898632)
