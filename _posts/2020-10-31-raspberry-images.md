---
layout: post
title:  "Raspberry images"
date:   2020-10-31
tags: [Raspberry Pi]
---


We use raspberry pi for out students to give an introduction to working with linux and electronics.

The default raspberry pi OS image is nice, and comes in [multiple flavors](https://www.raspberrypi.org/downloads/raspberry-pi-os/). It may come as a surprise, but I always chose the lite version without gui.

## background

The problem I faced was how to handle the default setup of images when I needed a new one. It always included installng a couple of programs, some config tweaking, setting a static ip and putting my public key on the device.

This is, of course, very automatable.

It could spin up the image in a virtual machine, do my stuff and save it as the new image.
This has at least two drawbacks:
* The default image has this cool trick were it automatically resizes the partitions based on the size of the SD card. So booting from the image would be nove to avoid.
* Actually running virtual machines is  problem for CI. It is resource heavy and I have not had good success with it neither in my own nor in te supplies runnners on gitlab.com.

The solution is to use [`proot`](https://proot-me.github.io/), which is kind of a chroot except it also handles mounting stuff and permissions. It also allows for ARM binaries to be run transparently on my x86 server.

## Usage

There are two files to edit, it supports `make` and gitlab-ci out-of-the-box. So fork it and do your testing.

Read the [readme](https://gitlab.com/moozer/raspberry-image-static/-/blob/master/readme.md) for more info.

Note that default is to put the image on gitlab pages.

The project is [here](https://gitlab.com/moozer/raspberry-image-static).
