---
layout: post
title:  "Ansible vault pass script"
date:   2021-11-29
tags: [Ansible, Vault, Secrets]
---

Ansible supports the notion of vaults, ie. encrypted files, that is decrypted on-the-fly. It support both for playbooks and for vars-files - we focus on the latter.

The official documentation is [here](https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html)

## Plain file secret

Make a file with the password and use `--vault-password-file <file>` to give ansible access to the file.

## Scripted

The specific documentation is [here](https://docs.ansible.com/ansible/latest/vault_guide/vault_using_encrypted_content.html)

1. Create a file called `*-client.sh`, `*-client.py` or something similar
2. Make it return the password on stdout
3. make it executable
4. Specify it the same way as with the plaint text file.`--vault-password-file <file>`

It has the nice advantage that the script can pull secrets from where ever, like other password vaults, APIs, plain text files and so on.

Ansible support `--vault-id` which acts as a selector, and the scripts will get the vault id as parameter.

An example script is on gitlab.

{% assign snippet_id=2213235 %}
{% include gitlab-snippet.html snippet_id=2213235 %}

To use in `ansible.fg`, add

```
[defaults]
vault_identity_list = id_A@secrets-client.sh,id_B@secrets-client.sh
``` 

if `id` is omitted, the secret named `default` is used.


Update 220422: add ansible.cfg comment and fix bug in gist.