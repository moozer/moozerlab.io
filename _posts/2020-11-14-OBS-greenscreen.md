---
layout: post
title:  "Greenscreen on OBS studio"
date:   2020-11-14
tags: [Greenscreen, OBS studio]
---

I use [OBS studio](https://obsproject.com/) for creating videos and live stuff.

Setting up a greenscren has some challenges, but in the end a fairly simple solution. I follow the suggestion from [Voorny](https://www.youtube.com/watch?v=07Vg6kmFrzA)

He made a [follow-up video](https://www.youtube.com/watch?v=kBj4zY5RmGw) where he picks the "green" color from he backgound and uses that. That works also, but I prefer the first version.

## Howto

The physical setup is to have a bright green backdrop and some lighting. The trick ensures that it is not necesary to have perfect lighting, just enough.

Create a new scene  called e.g. `greenscreen-camera`
1. Add the video capture source
2. Add a very green backgound `color source`, eg. `#f00ff00`

Add filters to captured video
1. Add `crop/pad`, if applicable
2. Add `color correction`
3. Add `chroma key` and select color `greeen`
4. Adjust the color correction values, so the green screen effect is good. Ignore miscoloring of the capmera input

  * My current values: gamma -0.36, contrast 0.0, brightness 0.0, saturation 0.67, Hue shift 0 and opacity 100

Add filter to the scene
1. Add `color correction` filter to the `greenscreen-camera`
2. Adjust to make camera input look good
