---
layout: post
title:  "PDF project"
date:   2019-06-08
tags: [PDF, Gitlab-CI, Docker]
---

I have been working on PDF generation from markdown for a while.

The usage guide is [here](https://moozer.gitlab.io/document_templates/pdf_guide)

The current status for the system:
* We have a docker image to use with gitlab-ci to avoid downloading and installing stuff everytime. Since we use LateX, it is around 1GB of stuff to install, so it takes time that we want to avoid. Docker run from `registry.gitlab.com/moozer/docker-pdf:latest` - it is currently autoupdated every month.
* The docker image comes in multiple flavours, see the [project readme](https://gitlab.com/moozer/docker-pdf/blob/master/readme.md)
* The actual template is avaible [here](https://gitlab.com/moozer/document_templates/). Check the [readme](https://gitlab.com/moozer/document_templates/blob/master/readme.md)

It works and gives us nice UCL templated PDF documents.

Issues
* ~~Using submodules are a bad design decision. It will be changed to some downloadable .tz file at some point. Submodules is cumbersome and not userfriendly for most people.~~ We now have releases and it requires download of two tgz-files.
* We have *one* template for UCL and no easy way of adding and seleting others.
* The LateX template is an `article` class with lots of stuff in the file. The proper LateX solution is to make our own *class*. That would solve the issue of how to select other templates.
* We also want the internal date in the PDFs to be the date of the last commit
