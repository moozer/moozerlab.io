---
layout: main
title: Links
permalink: /links/
---

Some

* [Twitter](https://twitter.com/moozer)

My courses, video collections and such

* [Networking basics](https://moozer.gitlab.io/course-networking-basisc/)
* [Videos on youtube](https://www.youtube.com/channel/UCLXmocfTGaxxRv3M3g7WjLA/videos)

