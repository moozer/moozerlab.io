---
layout: post
title:  Gitlab releases
date:   2019-12-15
tags: [Gitlab, Releases]
---

I have set up gitlab releases previously as part of the [PDF generation project]({% post_url 2019-06-08-pdf-project %}). The versions are on [gitlab as releases](https://gitlab.com/moozer/document_templates/-/releases).

The [release API](https://docs.gitlab.com/ee/api/releases/) is a bit cumbersome, and takes some getting used to. On the other, it is designed for automation and to be used with CI.

To use you need to know project id and the relevant tag. Gitlab CI supplies this as environemtn variables. You also need [an API key](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), which is not supplied, and you must [generate your own](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token).

I have made a [release test project](https://gitlab.com/moozer/release-test) to illustrate how to use it. It includes `curl` command examples.

Update 2020-02-10: The repo now includes stuff related to `uploads` that I suggest using instead of build artifacts.

Update 2022-04-15: Gitlab made changes to some docs. A new reference related to API keys/job-tokens and permissions is [here](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html)